One thing I've recently found out about is the `setvtrgb` command from the `kbd` package.  
It's an awesome command that allows you to set default colors for your VT (as inferred by the name).  
This is a collection of files you can use for `setvtrgb`

One thing not mentioned by the manpage is that you can actually set these colors by way of kernel parameters.  
In your `/etc/default/grub`, append these kernel parameters to `GRUB_CMDLINE_LINUX_DEFAULT`:
```
vt.default_red=40,204,151,215,69,177,104,168,148,251,184,250,131,211,142,235 vt.default_grn=40,36,151,153,133,98,156,153,131,73,187,189,165,134,192,219 vt.default_blu=40,29,26,33,136,134,106,132,116,52,38,47,152,155,124,178
```
